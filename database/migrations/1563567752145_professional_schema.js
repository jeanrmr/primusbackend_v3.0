'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProfessionalSchema extends Schema {
  up () {
    this.create('professionals', (table) => {
      table.increments()
      table.string('facebook', 256)
      table.string('instagram', 256)
      table.string('bio', 300)
      table.integer('user_id', 11).unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('professionals')
  }
}

module.exports = ProfessionalSchema
