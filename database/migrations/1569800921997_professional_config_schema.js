'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProfessionalConfigSchema extends Schema {
  up () {
    this.create('professional_configs', (table) => {
      table.increments()
      table.time('interval_init_time').notNullable()
      table.integer('interval_amount_time').notNullable()
      table.integer('professional_id', 11).notNullable().unsigned().references('id').inTable('professionals')
      table.timestamps()
    })
  }

  down () {
    this.drop('professional_configs')
  }
}

module.exports = ProfessionalConfigSchema
