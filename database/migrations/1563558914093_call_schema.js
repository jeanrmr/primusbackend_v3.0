'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CallSchema extends Schema {
  up () {
    this.create('calls', (table) => {
      table.increments()
    })
  }

  down () {
    this.drop('calls')
  }
}

module.exports = CallSchema
