'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ServiceProfessionalSchema extends Schema {
  up () {
    this.create('service_professionals', (table) => {
      table.increments()
      table.integer('time', 999).notNullable()
      table.integer('service_id', 11).unsigned().references('id').inTable('services')
      table.integer('professional_id', 11).unsigned().references('id').inTable('professionals')
      table.timestamps()
    })
  }

  down () {
    this.drop('service_professionals')
  }
}

module.exports = ServiceProfessionalSchema
