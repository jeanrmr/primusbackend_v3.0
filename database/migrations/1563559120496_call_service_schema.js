'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CallServiceSchema extends Schema {
  up () {
    this.create('call_services', (table) => {
      table.integer('id_service', 11).unsigned().references('id').inTable('services')
      table.integer('id_call', 11).unsigned().references('id').inTable('calls')
      table.increments()
    })
  }

  down () {
    this.drop('call_services')
  }
}

module.exports = CallServiceSchema
