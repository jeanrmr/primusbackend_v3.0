'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AppConfigSchema extends Schema {
  up () {
    this.create('app_configs', (table) => {
      table.increments()
      table.integer('interval_between_schedules').notNullable()
      table.time('default_interval_init_time').notNullable()
      table.integer('default_interval_amount_time').notNullable()
      table.time('business_hour_start').notNullable()
      table.time('business_hour_end').notNullable()
      table.boolean('sunday').notNullable()
      table.boolean('monday').notNullable()
      table.boolean('tuesday').notNullable()
      table.boolean('wednesday').notNullable()
      table.boolean('thursday').notNullable()
      table.boolean('friday').notNullable()
      table.boolean('saturday').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('app_configs')
  }
}

module.exports = AppConfigSchema
