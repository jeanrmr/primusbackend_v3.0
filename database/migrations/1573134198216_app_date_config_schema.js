'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AppDateConfigSchema extends Schema {
  up () {
    this.create('app_date_configs', (table) => {
      table.increments()
      table.date('date').notNullable()
      table.boolean('off').defaultTo(false)
      table.time('business_hour_start')
      table.time('business_hour_end')
      table.timestamps()
    })
  }

  down () {
    this.drop('app_date_configs')
  }
}

module.exports = AppDateConfigSchema
