'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProfessionalDateConfigSchema extends Schema {
  up () {
    this.create('professional_date_configs', (table) => {
      table.increments()
      table.date('date').notNullable()
      table.boolean('off').notNullable()
      table.time('interval_init_time').notNullable()
      table.integer('interval_amount_time').notNullable()
      table.time('business_hour_start').notNullable()
      table.time('business_hour_end').notNullable()
      table.integer('professional_config_id', 11).notNullable().unsigned().references('id').inTable('professional_configs')
      table.timestamps()
    })
  }

  down () {
    this.drop('professional_date_configs')
  }
}

module.exports = ProfessionalDateConfigSchema
