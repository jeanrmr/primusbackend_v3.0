'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/Service', async (faker) => {
  return {
    name: faker.word({ syllables: 3 }),
    price: faker.floating({ fixed: 2, min: 0, max: 100 })
  }
})

Factory.blueprint('App/Models/User', async (faker) => {
  return {
    firstName: faker.first(),
    lastName: faker.last(),
    email: faker.email(),
    password: faker.string({ length: 10 }),
    phone: faker.string({ length: 11, numeric: true }),
    oneSignalId: '1234'
  }
})

Factory.blueprint('App/Models/Professional', async (faker) => {
  return {
    facebook: faker.url({ domain: 'www.facebook.com' }),
    instagram: faker.url({ domain: 'www.instagram.com' }),
    bio: faker.paragraph({ sentences: 1 })
  }
})

Factory.blueprint('App/Models/ProfessionalConfig', async (faker) => {
  let hour = String(faker.hour())
  hour = hour.length === 1 ? '0' + hour : hour
  let minutes = String(faker.minute())
  minutes = minutes.length === 1 ? '0' + minutes : minutes

  return {
    interval_init_time: hour + ':' + minutes + ':00',
    interval_amount_time: faker.integer({ min: 1, max: 120 }),
    date: faker.date({ hour: 0, minute: 0, second: 0 })
  }
})
