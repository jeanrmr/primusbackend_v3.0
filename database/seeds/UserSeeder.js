'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const User = use('App/Models/User')
const Env = use('Env')

class UserSeeder {
  async run () {
    try {
      const defaultUser = new User()
      defaultUser.phone = '00000000000'
      defaultUser.firstName = 'admin'
      defaultUser.lastName = 'master'
      defaultUser.email = 'admin@email.com'
      defaultUser.type = '1'
      defaultUser.password = Env.get('DEFAULT_USER_PASSWORD')
      await defaultUser.save()

      const userUser = new User()
      userUser.phone = '22222222222'
      userUser.firstName = 'user'
      userUser.lastName = 'user'
      userUser.email = 'user@user.com'
      userUser.type = '2'
      userUser.password = '222222'
      await userUser.save()
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = UserSeeder
