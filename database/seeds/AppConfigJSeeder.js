'use strict'

/*
|--------------------------------------------------------------------------
| AppConfigJSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const AppConfig = use('App/Models/AppConfig')

class AppConfigJSeeder {
  async run() {
    const defaultConfig = new AppConfig()
    defaultConfig.interval_between_schedules = 10
    defaultConfig.default_interval_init_time = '12:00:00'
    defaultConfig.default_interval_amount_time = 60
    defaultConfig.business_hour_start = '08:00:00'
    defaultConfig.business_hour_end = '19:00:00'
    defaultConfig.sunday = false
    defaultConfig.monday = true
    defaultConfig.tuesday = true
    defaultConfig.wednesday = true
    defaultConfig.thursday = true
    defaultConfig.friday = true
    defaultConfig.saturday = true
    await defaultConfig.save()
  }
}

module.exports = AppConfigJSeeder
