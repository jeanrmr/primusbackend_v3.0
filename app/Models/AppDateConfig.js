'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class AppDateConfig extends Model {
    static get dates() {
        return super.dates.concat(['date'])
    }
}

module.exports = AppDateConfig
