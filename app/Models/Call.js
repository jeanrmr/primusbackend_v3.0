'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Call extends Model {
  services () {
    return this.manyThrough('App/Models/CallService', 'call')
  }
}

module.exports = Call
