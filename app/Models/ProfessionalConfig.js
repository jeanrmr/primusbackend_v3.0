'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProfessionalConfig extends Model {
  static get dates () {
    return super.dates.concat(['date'])
  }

  professional () {
    return this.belongsTo('App/Models/Professional')
  }
}

module.exports = ProfessionalConfig
