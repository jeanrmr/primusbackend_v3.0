'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CallService extends Model {
  service () {
    return this.belongsTo('App/Models/Service')
  }

  call () {
    return this.belongsTo('App/Models/Call')
  }
}

module.exports = CallService
