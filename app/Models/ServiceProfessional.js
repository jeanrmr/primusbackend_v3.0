'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ServiceProfessional extends Model {
  service () {
    return this.belongsTo('App/Models/Service')
  }

  professional () {
    return this.belongsTo('App/Models/Professional')
  }
}

module.exports = ServiceProfessional
