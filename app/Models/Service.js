'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Service extends Model {
  getPrice (price) {
    return price.toFixed(2)
  }

  calls () {
    return this.manyThrough('App/Models/CallService', 'service')
  }
}

module.exports = Service
