'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Professional extends Model {
  user () {
    return this.belongsTo('App/Models/User')
  }

  services () {
    return this.hasMany('App/Models/ServiceProfessional')
  }

  configs () {
    return this.hasMany('App/Models/ProfessionalConfig')
  }
}

module.exports = Professional
