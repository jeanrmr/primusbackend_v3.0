/* eslint-disable camelcase */
class Models {
  static removeModelProps (model) {
    if (Array.isArray(model)) {
      return model.map((item) => {
        const {
          updated_at,
          created_at,
          ...result
        } = item
        return result
      })
    } else {
      const {
        updated_at,
        created_at,
        ...result
      } = model
      return result
    }
  }
}

module.exports = Models
