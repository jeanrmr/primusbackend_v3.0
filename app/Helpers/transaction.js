const Database = use('Database')

module.exports = async function (func) {
  const isGlobalTrans = Database._globalTrx != null
  const trx = isGlobalTrans ? Database._globalTrx() : await Database.beginTransaction()
  try {
    await func(trx)
  } catch (error) {
    if (!isGlobalTrans) trx.rollback()
    throw error
  }
  if (!isGlobalTrans) trx.commit()
}
