/** @type {typeof import('./node_modules/@adonisjs/lucid/src/Lucid/Model')} */
const User = use('App/Models/User')

class UserService {
  async create (user, trx) {
    return User.create(user, trx)
  }

  async update (user, trx) {
    const { id: userId, ...requestUser } = user
    const pUser = await User.find(user.id)
    pUser.merge(requestUser)
    return pUser.save(trx)
  }
}

module.exports = new UserService()
