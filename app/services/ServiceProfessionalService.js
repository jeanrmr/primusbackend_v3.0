'use strict'

/** @type {typeof import('./node_modules/@adonisjs/lucid/src/Lucid/Model')} */
const ServiceProfessional = use('App/Models/ServiceProfessional')

class ServiceProfessionalService {
  async create (user, trx) {
    return ServiceProfessional.create(user, trx)
  }

  async createMany (user, trx) {
    return ServiceProfessional.create(user, trx)
  }
}

module.exports = new ServiceProfessionalService()
