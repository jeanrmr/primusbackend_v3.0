'use strict'

/** @type {typeof import('./node_modules/@adonisjs/lucid/src/Lucid/Model')} */
const Service = use('App/Models/Service')

class ServiceService {
  async create (user, trx) {
    return Service.create(user, trx)
  }

  async all () {
    return Service.all()
  }

  async show (id) {
    return Service.find(id)
  }

  async update (id, service, trx) {
    const oldService = await Service.find(id)
    oldService.merge(service)
    return oldService.save(trx)
  }

  async destroy (id, trx) {
    const service = await Service.find(id)
    if (service) {
      return service.delete(trx)
    } else {
      throw new Error('Service não encontrado')
    }
  }
}

module.exports = new ServiceService()
