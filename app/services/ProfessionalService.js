'use strict'

/** @type {typeof import('./node_modules/@adonisjs/lucid/src/Lucid/Model')} */
const Professional = use('App/Models/Professional')

/** @type {typeof import('./node_modules/@adonisjs/lucid/src/Lucid/Model')} */
const ServiceProfessional = use('App/Models/ServiceProfessional')

const UserService = use('App/Services/UserService')

class ProfessionalService {
  async index () {
    return Professional.query().with('user').fetch()
  }

  async store (user, professional, services, trx) {
    const pUser = await UserService.create(user, trx)
    const pProfessional = await pUser.professionals().create(professional, trx)
    if (services && services.length) {
      await pProfessional.services().createMany(services, trx)
    }
    return professional
  }

  async show (id) {
    return Professional.query().where('id', id).with('user').fetch()
  }

  async update (user, professional, services, trx) {
    const pProfessional = await Professional.query().transacting(trx).where('id', professional.id).first()
    await pProfessional.load('services')
    const pServices = await pProfessional.getRelated('services') || []

    const { id: professionalId, ...requestProfessional } = professional
    pProfessional.merge(requestProfessional)

    await UserService.update(user)

    const requestServices = services || []

    requestServices.forEach(async (rs) => {
      if (rs.id) {
        const serviceProfessional = await ServiceProfessional.find(rs.id)
        const { id, ...obj } = rs
        serviceProfessional.merge(obj)
        await serviceProfessional.save(trx)
      } else {
        await ServiceProfessional.create(rs, trx)
      }
    })

    pServices.rows.forEach(async (service) => {
      const match = requestServices.find((rs) => rs.id === service.id)
      if (!match) {
        await service.delete(trx)
      }
    })

    await pProfessional.save(trx)
  }

  async destroy (id, trx) {
    return Professional.delete(id, trx)
  }
}

module.exports = new ProfessionalService()
