'use strict'

/** @type {typeof import('./node_modules/@adonisjs/lucid/src/Lucid/Model')} */
const ProfessionalConfig = use('App/Models/ProfessionalConfig')

class ProfessionalConfigService {
  async getGenericProfessionalConfigsByProfId (profId) {
    return ProfessionalConfig.query().whereNull('date').where('professional_id', profId).fetch()
  }

  async getDailyProfessionalConfigsByProfId (profId) {
    return ProfessionalConfig.query().whereNotNull('date').where('professional_id', profId).fetch()
  }

  async getProfessionalConfigsById (profId) {
    return {
      generic: await this.getGenericProfessionalConfigsByProfId(profId),
      daily: await this.getDailyProfessionalConfigsByProfId(profId)
    }
  }
}

module.exports = new ProfessionalConfigService()
