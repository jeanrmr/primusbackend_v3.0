'use strict'
const User = use('App/Models/User')

class SessionController {
  async create ({ request, response, auth }) {
    const { phone, password, oneSignalId } = request.all()
    if (!phone || !password) {
      throw new Error('Informar Telefone com DDD e senha!')
    }
    const user = await User.findBy('phone', phone)
    if (!user) return response.status(401).send('Usuário não encontrado!')
    try {
      const token = await auth.attempt(phone, password)
      user.oneSignalId = oneSignalId
      await user.save()
      return response.status(200).send({ token: token.token, ...user.$attributes, password: undefined })
    } catch (error) {
      if (error.code === 'E_PASSWORD_MISMATCH') { return response.status(401).send('Senha incorreta!') }
      response.status(500).send(error)
    }
  }
}

module.exports = SessionController
