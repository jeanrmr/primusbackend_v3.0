'use strict'
/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const User = use('App/Models/User')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class UserController {
  async store ({ request, response, auth }) {
    try {
      const data = request.all()
      data.type = User.types.CLIENT
      const user = await User.create(data)
      const token = await auth.generate(user)
      delete user.$attributes.password
      return { ...user.$attributes, token }
    } catch (error) {
      return response.status(500).send(error)
    }
  }

  async index () {
    return User.all()
  }

  async show ({ request }) {
    return User.find(request.params.id)
  }

  /**
   * Update professional details.
   * PUT or PATCH users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response, auth }) {
    try {
      const logedUser = await auth.getUser()
      const userId = logedUser.type === User.types.ADMIN ? params.id : logedUser.id
      const user = await User.query().where('id', userId).first()
      const newUserInfo = request.all()
      user.merge({
        ...newUserInfo,
        password: newUserInfo.password ? newUserInfo.password : user.password
      })
      return user.save()
    } catch (error) {
      return response.status(500).send(error)
    }
  }
}

module.exports = UserController
