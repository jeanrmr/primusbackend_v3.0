'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Professional = use('App/Models/Professional')

const transaction = use('App/Helpers/transaction')

const professionalService = use('App/Services/ProfessionalService')

/**
 * Resourceful controller for interacting with professionals
 */
class ProfessionalController {
  /**
   * Show a list of all professionals.
   * GET professionals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    return Professional.query().with('user').fetch()
  }

  /**
   * Create/save a new professional.
   * POST professionals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ params, request, response }) {
    const user = request.only('user').user
    const professional = request.except(['user', 'services'])
    const services = request.only('services').services

    await transaction(async (trx) => {
      response.status(200).send(await professionalService.store(user, professional, services, trx))
    })
  }

  /**
   * Display a single professional.
   * GET professionals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    return professionalService.show(params.id)
  }

  /**
   * Update professional details.
   * PUT or PATCH professionals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const user = request.only('user').user
    const professional = request.except(['user', 'services'])
    const services = request.only('services').services

    await transaction(async (trx) => {
      response.status(200).send(await professionalService.update(user, professional, services, trx))
    })
  }

  /**
   * Delete a professional with id.
   * DELETE professionals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ProfessionalController
