'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const AppDateConfig = use('App/Models/AppDateConfig')

const moment = use('moment')

class AppDateConfigController {
  /**
   * Display a single professional.
   * GET appDateConfig/:date
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async findByDate ({ params, request, response }) {
    const { date } = request.qs
    return AppDateConfig.findBy('date', moment(date, 'DD-MM-YYYY').toDate())
  }

  /**
   * Create/save a new AppDateConfig.
   * POST clients
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }
}

module.exports = AppDateConfigController
