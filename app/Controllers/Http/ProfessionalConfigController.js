'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const ProfessionalConfig = use('App/Models/ProfessionalConfig')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const User = use('App/Models/User')

const ProfessionalConfigService = use('App/Services/ProfessionalConfigService')

const Database = use('Database')

class ProfessionalConfigController {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    await Database.transaction(async (trx) => {
      const professionalConfig = await ProfessionalConfig.find(params.id)
      professionalConfig.merge(request.all())
      await professionalConfig.save()
      response.status(200)
    })
  }

  /**
   * rules: if request contains a date, find by date and if exists, update him. If the request
   * dont contains a date, update the general config, If dont exists, create it!
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ params, request, response }) {
    const config = request.all()
    if (config.date) {
      const pConfig = await ProfessionalConfig.findBy('date', config.date)
      if (pConfig) {
        pConfig.merge(config)
        await pConfig.save()
      } else {
        response.status(200).send(await ProfessionalConfig.create(config))
      }
    } else {
      const pConfig = await ProfessionalConfig.findBy('date', null)
      pConfig.merge(config)
      await pConfig.save()
      response.status(200)
    }
    const professionalConfig = request.all()
    response.status(200).send(await ProfessionalConfig.create(professionalConfig))
  }

  /**
   * if is a professional, return the list of his configs, if a admin, return
   * the general list of configs.
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ params, request, response, auth }) {
    const user = await auth.getUser()
    if (user.type === User.types.PROFESSIONAL) {
      return ProfessionalConfigService.getProfessionalConfigsById(user.id)
    } else {

    }
  }
}

module.exports = ProfessionalConfigController
