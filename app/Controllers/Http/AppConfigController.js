'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const AppConfig = use('App/Models/AppConfig')

const Database = use('Database')

class AppConfigController {
  /**
   * Show a list of all professionals.
   * GET professionals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    return AppConfig.first()
  }

  /**
   * Update professional details.
   * PUT or PATCH professionals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    await Database.transaction(async (trx) => {
      const appConfig = await AppConfig.first()
      appConfig.merge(request.all())
      await appConfig.save(trx)
      response.send(await AppConfig.first())
    })
  }
}

module.exports = AppConfigController
