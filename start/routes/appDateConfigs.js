const Route = use('Route')
const Env = use('Env')

// const protectedUrls = Route.resource('/appDateConfig', 'AppDateConfigController').apiOnly()

const getByDate = Route.get('/appDateConfigs/findByDate', 'AppDateConfigController.findByDate')

if (Env.get('NODE_ENV') !== 'development') {
  // protectedUrls.middleware(['auth'])
  getByDate.middleware(['auth'])
}
