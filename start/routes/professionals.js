const Route = use('Route')
const Env = use('Env')

const protectedUrls = Route.resource('/professionals', 'ProfessionalController').apiOnly()

if (Env.get('NODE_ENV') !== 'development') {
  protectedUrls.middleware(['auth'])
}
