const Route = use('Route')
const Env = use('Env')

const protectedUrls = Route.resource('/users', 'UserController').apiOnly().except('store')
if (Env.get('NODE_ENV', 'development') !== 'development') {
  protectedUrls.middleware(['auth'])
}

// register user
Route.post('/users', 'UserController.store')
