const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Welcome to primus API!' }
})

Route.group(() => {
  require('./users')
  require('./session')
  require('./services')
  require('./professionals')
  require('./appConfigs')
  require('./professionalConfigs')
  require('./appDateConfigs')
}).prefix('/primus/api')
