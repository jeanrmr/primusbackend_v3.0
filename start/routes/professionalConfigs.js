const Route = use('Route')
const Env = use('Env')

const protectedUrls = Route.resource('/professionalConfigs', 'ProfessionalConfigController').apiOnly()

if (Env.get('NODE_ENV') !== 'development') {
  protectedUrls.middleware(['auth'])
}
