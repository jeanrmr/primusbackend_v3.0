'use strict'

const { test, trait, before } = use('Test/Suite')('User Spec Js')
/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const User = use('App/Models/User')
/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

const modelsHelper = use('App/Helpers/Models')

trait('Test/ApiClient')
trait('Auth/Client')
trait('DatabaseTransactions')

let admin

before(async () => {
  admin = await User.find(1)
  const client = await Factory.model('App/Models/User').make()
  client.type = User.types.CLIENT
  await client.save()
})

test('signup', async ({ assert, client }) => {
  const newUser = await Factory.model('App/Models/User').make()
  const response = await client.post('/primus/api/users').send({
    ...modelsHelper.removeModelProps(newUser.toJSON()),
    password: newUser.password
  }).end()
  response.assertStatus(200)
  assert.containsAllKeys(response.body, ['token', 'phone', 'firstName', 'lastName', 'type'])
  const persistedUser = await User.last()
  assert.include(modelsHelper.removeModelProps(persistedUser.toJSON()), newUser.toJSON())
})

test('list', async ({ assert, client }) => {
  const users = await User.all()
  const response = await client.get('/primus/api/users').send().loginVia(admin, 'jwt').end()
  assert.equal(
    JSON.stringify(modelsHelper.removeModelProps(users.toJSON())),
    JSON.stringify(modelsHelper.removeModelProps(response.body))
  )
})

test('admin update user CLIENT', async ({ assert, client }) => {
  let user = await User.last()
  const newUserInfo = await Factory.model('App/Models/User').make()
  user.merge({
    ...modelsHelper.removeModelProps(newUserInfo.toJSON())
  })
  const response = await client.put('/primus/api/users/' + user.id).send(user.toJSON()).loginVia(admin, 'jwt').end()
  user = await User.last()
  response.assertStatus(200)
  assert.include(
    modelsHelper.removeModelProps(user.toJSON()),
    modelsHelper.removeModelProps(newUserInfo.toJSON())
  )
})

test('user update own profile', async ({ assert, client }) => {
  let user = await User.query().where('type', User.types.CLIENT).last()
  const response = await client.put('/primus/api/users/' + user.id).send({
    firstName: 'My new name'
  }).loginVia(user, 'jwt').end()
  user = await User.query().where('type', User.types.CLIENT).last()
  response.assertStatus(200)
  assert.include(
    modelsHelper.removeModelProps(user.toJSON()),
    { firstName: 'My new name' }
  )
})

test('user update password', async ({ assert, client, auth }) => {
  const user = await User.query().where('type', User.types.CLIENT).last()
  const newPassword = 'My new password'
  const response = await client.put('/primus/api/users/' + user.id).send({
    firstName: 'My new name',
    password: newPassword
  }).loginVia(user, 'jwt').end()
  await user.reload()
  response.assertStatus(200)
  assert.equal(await Hash.verify(newPassword, user.password), true)
})
