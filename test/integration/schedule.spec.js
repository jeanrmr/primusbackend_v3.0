'use strict'

const { test, trait, before } = use('Test/Suite')('Schedule')
/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const User = use('App/Models/User')

trait('Auth/Client')
trait('DatabaseTransactions')

let admin

before(async () => {
  admin = await User.find(1)
})

test('get avaliable time by one professional', async ({ assert, auth }) => {
  const prof = await User.find(2)
  // call getAvaliableHour with params: date, timeAmount, profArray
})

test('get avaliable time many professionals', async ({ assert, auth }) => {

})

test('get avaliable time of all professionals', async ({ assert, auth }) => {

})
