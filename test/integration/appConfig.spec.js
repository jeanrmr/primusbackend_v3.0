'use strict'

const { test, trait, before } = use('Test/Suite')('AppConfig')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const User = use('App/Models/User')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const AppConfig = use('App/Models/AppConfig')

trait('Test/ApiClient')
trait('Auth/Client')
trait('DatabaseTransactions')

let admin

before(async () => {
  admin = await User.find(1)
})

test('get config', async ({ assert, client }) => {
  const defaultConfig = await AppConfig.first()
  const response = await client.get('/primus/api/appConfigs')
    .loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  const configs = response.body
  assert.include(defaultConfig.toJSON(), configs)
})

test('update amount of time between each schedule', async ({ assert, client }) => {
  const config = await AppConfig.first()
  const newInterval = config.interval_between_schedules + 1
  const response = await client.put('/primus/api/appConfigs/' + config.id)
    .send({ ...config.toJSON(), interval_between_schedules: newInterval })
    .loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  await config.reload()
  assert.equal(config.interval_between_schedules, newInterval)
})

test('update default_interval_init_time', async ({ assert, client }) => {
  const config = await AppConfig.first()
  const newTime = '12:01:00'
  const response = await client.put('/primus/api/appConfigs/' + config.id)
    .send({ ...config.toJSON(), default_interval_init_time: newTime })
    .loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  await config.reload()
  assert.equal(config.default_interval_init_time, newTime)
})

test('update default_interval_amount_time', async ({ assert, client }) => {
  const config = await AppConfig.first()
  const newAmount = config.default_interval_amount_time + 1
  const response = await client.put('/primus/api/appConfigs/' + config.id)
    .send({ ...config.toJSON(), default_interval_amount_time: newAmount })
    .loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  await config.reload()
  assert.equal(config.default_interval_amount_time, newAmount)
})

test('update business hour', async ({ assert, client }) => {
  const config = await AppConfig.first()
  const newStartHour = '08:01:00'
  const newEndHour = '18:59:00'
  const response = await client.put('/primus/api/appConfigs/' + config.id)
    .send(
      {
        ...config.toJSON(),
        business_hour_start: newStartHour,
        business_hour_end: newEndHour
      }
    )
    .loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  await config.reload()
  assert.equal(config.business_hour_start, newStartHour)
  assert.equal(config.business_hour_end, newEndHour)
})

test('update week days opened', async ({ assert, client }) => {
  const config = await AppConfig.first()
  config.monday = true
 
  const response = await client.put('/primus/api/appConfigs/' + config.id)
    .send(
      {
        ...config.toJSON()
      }
    )
    .loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  await config.reload()
  assert.equal(config.monday, true)
})

test('update interval_between_schedules, default_interval_init_time, default_interval_amount_time', async ({ assert, client }) => {
  const config = await AppConfig.first()

  const newInterval = config.interval_between_schedules + 1
  const newTime = '12:01:00'
  const newAmout = config.default_interval_amount_time + 1

  const response = await client.put('/primus/api/appConfigs/' + config.id)
    .send(
      {
        ...config.toJSON(),
        interval_between_schedules: newInterval,
        default_interval_init_time: newTime,
        default_interval_amount_time: newAmout,
      }
    )
    .loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  await config.reload()
  assert.equal(config.interval_between_schedules, newInterval)
  assert.equal(config.default_interval_init_time, newTime)
  assert.equal(config.default_interval_amount_time, newAmout)
})
