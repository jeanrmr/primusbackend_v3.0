'use strict'

const Env = use('Env')
const { test, trait } = use('Test/Suite')('Session')
const User = use('App/Models/User')

trait('Test/ApiClient')

test('signin', async ({ client, assert }) => {
  const admUser = await User.first()
  const response = await client.post('/primus/api/session').send({
    phone: admUser.phone,
    password: Env.get('DEFAULT_USER_PASSWORD'),
    oneSignalId: '1234'
  }).end()

  response.assertStatus(200)
  assert.containsAllKeys(response.body, ['token', 'phone', 'firstName', 'lastName', 'type'])
})
