'use strict'

const { test, trait, before } = use('Test/Suite')('ProfessionalConfig')
/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const User = use('App/Models/User')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const ProfessionalConfig = use('App/Models/ProfessionalConfig')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Professional = use('App/Models/Professional')

const ProfessionalConfigService = use('App/Services/ProfessionalConfigService')

trait('Test/ApiClient')
trait('Auth/Client')
trait('DatabaseTransactions')

let admin
let userProf

before(async () => {
  admin = await User.find(1)
  userProf = await User.find(2)

  // create 3 new services
  const services = await Factory.model('App/Models/Service').makeMany(3)
  await services.forEach(async (service) => { await service.save() })

  // create a new professional with 3 services
  const professional = await Factory.model('App/Models/Professional').make()
  professional.set('user_id', userProf.id)
  await professional.save()
  await professional.services().createMany(services.map(service => {
    service = service.toJSON()
    return {
      service_id: service.id,
      professional_id: professional.id,
      time: 10
    }
  }))

  await professional.reload()

  // create a general config to the professional
  const profConfGeneric = await Factory.model('App/Models/ProfessionalConfig').make()
  profConfGeneric.set('date', null)
  profConfGeneric.set('professional_id', professional.id)
  await ProfessionalConfig.create(profConfGeneric.toJSON())

  // create a specific day config to the professional
  const profDayConfig = await Factory.model('App/Models/ProfessionalConfig').make()
  profDayConfig.set('professional_id', professional.id)
  await ProfessionalConfig.create(profDayConfig.toJSON())
})

// test('list configs by loged professional', async ({ assert, auth, client }) => {
//   const response = await client.get('primus/api/professionalConfigs').loginVia(userProf, 'jwt').end()
//   response.assertStatus(200)
// })

test('list general configs', async ({ assert, auth, client }) => {
  const response = await client.get('primus/api/professionalConfigs').loginVia(userProf, 'jwt').end()
  response.assertStatus(200)
})

test('list general configs', async ({ assert, auth, client }) => {
  const response = await client.get('primus/api/professionalConfigs').loginVia(userProf, 'jwt').end()
  response.assertStatus(200)
})

// test('create professional config', async ({ assert, auth, client }) => {
//   const mockProfConfig = await Factory.model('App/Models/ProfessionalConfig').make()
//   mockProfConfig.set('date', null)
//   const prof = await Professional.first()
//   mockProfConfig.set('professional_id', prof.id)
//   const response = await client.post('/primus/api/professionalConfigs')
//     .send(mockProfConfig.toJSON())
//     .loginVia(prof, 'jwt').end()
//   response.assertStatus(200)
//   const newProfConfig = await ProfessionalConfig.last()
//   assert.include(newProfConfig.toJSON(), mockProfConfig.toJSON())
// })

// test('create to specific day', async ({ assert, auth, client }) => {
//   const mockProfConfig = await Factory.model('App/Models/ProfessionalConfig').make()
//   const prof = await Professional.first()
//   mockProfConfig.set('professional_id', prof.id)
//   const response = await client.post('/primus/api/professionalConfigs')
//     .send(mockProfConfig.toJSON())
//     .loginVia(prof, 'jwt').end()
//   response.assertStatus(200)
//   const newProfConfig = await ProfessionalConfig.last()
//   assert.include(newProfConfig.toJSON(), mockProfConfig.toJSON())
// })

// test('update a specic day', async ({ assert, auth, client }) => {
//   const mockConfig = await Factory.model('App/Models/ProfessionalConfig').make()
//   const pProfConfig = await ProfessionalConfig.find(2)

//   pProfConfig.set('interval_init_time', mockConfig.get('interval_init_time'))
//   pProfConfig.set('interval_amount_time', mockConfig.get('interval_amount_time'))
//   const response = await client.post('/primus/api/professionalConfigs')
//     .send(pProfConfig.toJSON())
//     .loginVia(userProf, 'jwt').end()

//   response.assertStatus(200)
//   await pProfConfig.reload()
//   assert.include(pProfConfig.toJSON(), mockConfig.toJSON())
// })

// test('delete a specific own day config', async ({ assert, auth, client }) => {
//   const pProfConfig = await ProfessionalConfig.last()
//   const response = await client.delete('/primus/api/professionalConfigs/' + pProfConfig.id)
//     .loginVia(userProf, 'jwt').end()

//   response.assertStatus(200)
//   await pProfConfig.reload()
//   assert.equal(pProfConfig.id, null)
// })

// test('delete a specific own day config', async ({ assert, auth, client }) => {
//   const pProfConfig = await ProfessionalConfig.last()
//   const response = await client.delete('/primus/api/professionalConfigs/' + pProfConfig.id)
//     .loginVia(userProf, 'jwt').end()

//   response.assertStatus(200)
//   await pProfConfig.reload()
//   assert.equal(pProfConfig.id, null)
// })
