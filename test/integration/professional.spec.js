'use strict'

const { test, trait, before } = use('Test/Suite')('Professional')
/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const modelsHelper = use('App/Helpers/Models')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const User = use('App/Models/User')
/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Professional = use('App/Models/Professional')
/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Service = use('App/Models/Service')

trait('Test/ApiClient')
trait('Auth/Client')
trait('DatabaseTransactions')

let admin

before(async () => {
  admin = await User.find(1)
  // create 3 new services
  const services = await Factory.model('App/Models/Service').makeMany(3)
  await services.forEach(async (service) => { await service.save() })

  // create a new professional with 3 services
  const user = await User.find(2)
  const professional = await Factory.model('App/Models/Professional').make()
  professional.set('user_id', user.id)
  await professional.save()
  await professional.services().createMany(services.map(service => {
    service = service.toJSON()
    return {
      service_id: service.id,
      professional_id: professional.id,
      time: 10
    }
  }))
})

test('create without services', async ({ client, assert }) => {
  const professional = await Factory.model('App/Models/Professional').make()
  const user = await Factory.model('App/Models/User').make()
  user.set('type', User.types.PROFESSIONAL)
  const response = await client.post('/primus/api/professionals')
    .send(
      {
        ...professional.toJSON(),
        user: {
          ...user.toJSON(),
          password: user.$attributes.password
        },
        services: []
      }
    )
    .loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  const newProfesional = (await Professional.pickInverse()).rows[0]
  const newUser = await (await User.pickInverse()).rows[0]
  assert.include(newProfesional.$attributes, professional.$attributes)
  delete user.$attributes.password
  assert.include(newUser.$attributes, user.$attributes)
}).timeout(99999)

test('create with services', async ({ client, assert }) => {
  const professional = await Factory.model('App/Models/Professional').make()
  const user = await Factory.model('App/Models/User').make()
  user.set('type', User.types.PROFESSIONAL)
  const services = await Service.all()
  const response = await client.post('/primus/api/professionals')
    .send(
      {
        ...professional.toJSON(),
        user: {
          ...user.toJSON(),
          password: user.$attributes.password
        },
        services: services.rows.map((service) => ({
          time: 30,
          service_id: service.$attributes.id
        }))
      }
    )
    .loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  const newProfesional = await Professional.last()
  const newUser = await User.last()
  await newProfesional.load('services')
  const serviceProfessionals = await newProfesional.getRelated('services')

  assert.include(newProfesional.toJSON(), professional.toJSON())
  assert.include(newUser.toJSON(), user.toJSON())
  serviceProfessionals.toJSON().forEach((sp) => {
    assert.equal(newProfesional.id, sp.professional_id)
  })
})

test('update only professional info', async ({ client, assert }) => {
  const professional = await Professional.last()
  await professional.loadMany(['user', 'services'])
  const user = await professional.getRelated('user')
  const services = await professional.getRelated('services')

  const changes = {
    facebook: professional.toJSON().facebook + 'a',
    instagram: professional.toJSON().instagram + 'a',
    bio: professional.toJSON().bio + 'a'
  }
  professional.merge(changes)
  const response = await client.put('/primus/api/professionals/' + professional.id)
    .send({
      ...professional.toJSON(),
      user: user.toJSON(),
      services: services.toJSON()
    }).loginVia(admin, 'jwt').end()
  const updatedProfessional = await Professional.find(professional.id)
  response.assertStatus(200)
  assert.include(updatedProfessional.toJSON(), changes)
})

test('update professional and user info', async ({ client, assert }) => {
  const professional = await Professional.last()
  await professional.load('user')
  const user = professional.getRelated('user')

  const professionalChanges = {
    facebook: 'asdfsadf',
    instagram: 'asdf',
    bio: 'bio'
  }
  const userChanges = {
    firstName: 'firstName', lastName: 'lastName', email: 'asdf@asdf.com'
  }

  professional.merge(professionalChanges)
  user.merge(userChanges)
  const response = await client.put('/primus/api/professionals/' + professional.id)
    .send(professional.toJSON()).loginVia(admin, 'jwt').end()
  const updatedProfessional = await Professional.last()
  await updatedProfessional.load('user')
  const updatedUser = updatedProfessional.getRelated('user')
  response.assertStatus(200)
  assert.include(updatedProfessional.toJSON(), professionalChanges)
  assert.include(updatedUser.toJSON(), userChanges)
})

test('update professional, user and service info', async ({ client, assert }) => {
  const professional = await Professional.last()
  await professional.loadMany(['user', 'services'])
  const user = await professional.getRelated('user')
  const services = await professional.getRelated('services')

  const professionalChanges = {
    facebook: 'asdf',
    instagram: 'asdf',
    bio: 'asdf'
  }

  const userChanges = {
    firstName: 'asdf', lastName: 'asdf', email: 'asdfasdf'
  }

  const servicesProfessionalChanges = services.toJSON().map(
    (value) =>
      ({ ...value, time: value.time + 1 })
  )

  professional.merge(professionalChanges)
  user.merge(userChanges)
  servicesProfessionalChanges.forEach((value, index) => {
    services.rows[index].merge(value)
  })
  const response = await client.put('/primus/api/professionals/' + professional.id)
    .send(professional.toJSON()).loginVia(admin, 'jwt').end()

  const updatedProfessional = await Professional.last()

  await updatedProfessional.loadMany(['user', 'services'])
  const updatedUser = await updatedProfessional.getRelated('user')
  const updatedServicesProfessinal = await updatedProfessional.getRelated('services')
  response.assertStatus(200)

  assert.include(
    modelsHelper.removeModelProps(updatedProfessional.toJSON()),
    modelsHelper.removeModelProps(professionalChanges)
  )
  assert.include(
    modelsHelper.removeModelProps(updatedUser.toJSON()),
    modelsHelper.removeModelProps(userChanges)
  )

  updatedServicesProfessinal.toJSON().forEach((sp) => {
    const change = servicesProfessionalChanges.find((change) => change.id === sp.id)
    assert.include(
      modelsHelper.removeModelProps(sp),
      modelsHelper.removeModelProps(change)
    )
  })
})
