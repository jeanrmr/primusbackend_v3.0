'use strict'

const { test, trait, before } = use('Test/Suite')('Services')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const User = use('App/Models/User')
/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Service = use('App/Models/Service')
const Factory = use('Factory')

trait('Test/ApiClient')
trait('Auth/Client')
trait('DatabaseTransactions')

let admin

before(async () => {
  admin = await User.find(1)
  await Factory.model('App/Models/Service').createMany(3)
})

test(' create', async ({ client, assert }) => {
  const { name, price } = await Factory.model('App/Models/Service').make()
  const response = await client.post('/primus/api/services').send({ name, price }).loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  const newService = await Service.pickInverse()
  assert.include(newService.rows[0].$attributes, { name, price })
})

test('update', async ({ client, assert }) => {
  let service = await Service.last()
  const newServiceName = service.name + 'a'
  const response = await client.put('/primus/api/services/' + service.id).send({ name: newServiceName }).loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  service = await Service.last()
  assert.equal(service.name, newServiceName)
})

test('listAll', async ({ client, assert }) => {
  const services = await Service.all()
  const response = await client.get('/primus/api/services').loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  response.assertJSONSubset(services.toJSON())
})

test('findOne', async ({ client, assert }) => {
  const service = await Service.last()
  const response = await client.get('/primus/api/services/' + service.id).loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  response.assertJSONSubset(service.toJSON())
})

test('delete', async ({ client, assert }) => {
  let service = await Service.last()
  const response = await client.delete('/primus/api/services/' + service.id).loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  service = await Service.find(service.id)
  assert.equal(service, null)
})
