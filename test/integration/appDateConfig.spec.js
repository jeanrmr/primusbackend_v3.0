'use strict'

const { test, trait, before } = use('Test/Suite')('AppDateConfig')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const User = use('App/Models/User')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const AppDateConfig = use('App/Models/AppDateConfig')

const moment = use('moment')

const datePattern = 'DD-MM-YYYY'

trait('Test/ApiClient')
trait('Auth/Client')
trait('DatabaseTransactions')

let admin

before(async () => {
  admin = await User.find(1)
  await AppDateConfig.create({
    date: new Date(),
    off: true
  })
})

test('get config by day, today', async ({ assert, client }) => {
  const response = await client.get('/primus/api/appDateConfigs/findByDate').query({ date: moment().format(datePattern) })
    .loginVia(admin, 'jwt').end()
  response.assertStatus(200)
  assert.include(response.body, {
    id: 1,
    off: true,
    business_hour_start: null,
    business_hour_end: null
  })
})

test('get config by day that doesnt exists', async ({ assert, client }) => {
  const response = await client.get('/primus/api/appDateConfigs/findByDate').query(
    { date: moment('12-11-2019', datePattern).format(datePattern) }
  ).loginVia(admin, 'jwt').end()
  response.assertStatus(204)
})
